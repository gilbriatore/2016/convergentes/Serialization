package br.edu.up;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializador {
	
	public static void main(String[] args) throws Exception {

		Produto produto = new Produto();
		produto.setId(1L);
		produto.setNome("Pneu");
		produto.setValor(300.25);
		
		//Grava em arquivo o objeto serializado.
		FileOutputStream fos = new FileOutputStream("produto.ser");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(produto);
		oos.close();
		
		//L� o arquivo e deserializa o objeto.
		FileInputStream fis = new FileInputStream("produto.ser");
		ObjectInputStream ois = new ObjectInputStream(fis);
		Produto produtoDeserializado = (Produto) ois.readObject();
		System.out.println(produtoDeserializado);
		ois.close();
	}
}