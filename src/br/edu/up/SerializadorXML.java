package br.edu.up;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class SerializadorXML {
	
	public static void main(String[] args) throws Exception {

		Produto produto = new Produto();
		produto.setId(1L);
		produto.setNome("Pneu");
		produto.setValor(300.25);
		
		//Criar o contexto.
		JAXBContext ctx = JAXBContext.newInstance(Produto.class);
		
		//Faz a serialização para XML.
		Marshaller marshaller = ctx.createMarshaller();
		marshaller.marshal(produto, new File("produto.xml"));
		
		//Faz a deserialização do objeto.
		Unmarshaller un = ctx.createUnmarshaller();
		Produto pDeserializado = (Produto) un.unmarshal(new File("produto.xml"));
		
		System.out.println(pDeserializado);
		
	}
}